#include "IQueue.h"
#include <queue>
#include <mutex>

using namespace std;

template <class T>
class LockQueue : public IQueue<T>
{
    private:
        mutex mutexInstance;
        queue<T> queueInstance;

    public:
        
        virtual bool isEmpty()
        {
            mutexInstance.lock();
            return queueInstance.empty();
            mutexInstance.unlock();
        }

        virtual void enqueue(T elementToQueue)
        {
            mutexInstance.lock();
            queueInstance.push(elementToQueue);
            mutexInstance.unlock();
        }

        virtual T dequeue()
        {
            mutexInstance.lock();
            T frontElement = queueInstance.front();
            queueInstance.pop();
            return frontElement;
            mutexInstance.unlock();
        }

        virtual int size()
        {
            mutexInstance.lock();
            return queueInstance.size();
            mutexInstance.unlock();           
        }
};