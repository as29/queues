#include <iostream>
#include "LockQueue.cpp"
#include <chrono>
#include <numeric>
#include <thread>
#include <assert.h>


using namespace std;
using namespace std::chrono;

template <class T>
void EnqueueNItems(IQueue<T> &queue, int n)
{
    for (int i = 0; i < n; i++)
    {
        queue.enqueue(2);
    }
}

double CreateNThreadsAndPerformKOperations(int n, int k)
{
    thread workers[n];

    LockQueue<int> lockedQueue;

    auto start = high_resolution_clock::now();
    // do some work
    for (int i = 0; i < n; i++)
    {
        workers[i] = thread(EnqueueNItems<int>, ref(lockedQueue), k);
    }

    for (int i = 0; i < n; i++)
    {
        workers[i].join();
    }    

    auto end = high_resolution_clock::now();
    
    assert(lockedQueue.size() == n*k);

    duration<double> diff = end-start;    
    return diff.count();
}

int main()
{
    cout<<CreateNThreadsAndPerformKOperations(100, 1000)<<endl;
    cout<<CreateNThreadsAndPerformKOperations(100, 100000)<<endl;
}