#ifndef IQUEUE_H
#define IQUEUE_H

template <class T>
class IQueue
{
    public:
        
        virtual ~IQueue()
        {

        }

        virtual bool isEmpty() = 0;
        virtual void enqueue(T elementToQueue) = 0;
        virtual T dequeue() = 0;
        virtual int size() = 0;
};

#endif