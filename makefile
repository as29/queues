CXX = g++
CXXFLAGS = -Wall -g -std=c++11 -pthread

BenchmarkingModule: BenchmarkingModule.o LockQueue.o
	$(CXX) $(CXXFLAGS) -o BenchmarkingModule BenchmarkingModule.o LockQueue.o 

BenchmarkingModule.o: BenchmarkingModule.cpp
	$(CXX) $(CXXFLAGS) -c BenchmarkingModule.cpp

LockQueue.o: IQueue.h
	$(CXX) $(CXXFLAGS) -c LockQueue.cpp

clean:
	rm *.o BenchmarkingModule